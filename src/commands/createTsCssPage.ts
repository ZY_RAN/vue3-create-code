import { InputBoxOptions, Uri, window } from "vscode";
import { viewTemplate } from "../templates/view.template";
import { cssTemplate } from "../templates/css.template";
import { interfaceTemplate } from "../templates/interface.template";
import { apiTemplate } from "../templates/api.template";
import { createFile, createMkdir } from "../utils/system";
import { compontentsTemplate } from "../templates/compontents.template";

export const createTsScssPage = (uri: Uri) => initPage(uri, "scss");

export const createTsLessPage = (uri: Uri) => initPage(uri, "less");

const initPage = async (uri: Uri, cssGenerate: string) => {
  const name = await promptForPageName();
  if (!name) return;
  const pageName = name.replace(/\s+/g, "");
  const baseUrl = uri.fsPath + "/" + pageName;
  await createMkdir(baseUrl);
  await createMkdir(baseUrl + "/components");
  await createFile(
    baseUrl + "/components/helloworld.vue",
    compontentsTemplate(pageName!, cssGenerate)
  );
  await createFile(
    baseUrl + "/" + pageName + ".vue",
    viewTemplate(pageName!, cssGenerate)
  );
  await createFile(
    baseUrl + "/" + pageName + "." + cssGenerate,
    cssTemplate(pageName!)
  );
  await createFile(
    baseUrl + "/" + pageName + ".interface.ts",
    interfaceTemplate(pageName!)
  );
  await createFile(
    baseUrl + "/" + pageName + ".api.ts",
    apiTemplate(pageName!)
  );
};

function promptForPageName(): Thenable<string | undefined> {
  const namePromptOptions: InputBoxOptions = {
    prompt: "请输入名称",
    // placeHolder: "counter",
  };
  return window.showInputBox(namePromptOptions);
}
