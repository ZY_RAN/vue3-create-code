export function viewTemplate(name: string, cssGenerate: string = "scss") {
  let str = `<template>
 <div class="${name}">
     <HelloWorld></HelloWorld>
     <div> 测试数据 </div>
 </div>
</template>

<script lang="ts" setup>
import { getApi } from './${name}.api';
import { moduleInterface } from './${name}.interface';
import HelloWorld from './components/helloworld.vue';
</script>


<style lang="${cssGenerate}" scoped src="./${name}.${cssGenerate}"></style>`;

  return str;
}
