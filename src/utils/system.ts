import * as fs from "node:fs";

// 创建文件
export const createFile = (url: string, context: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    fs.writeFile(url, context, (error: any) => {
      if (error) {
        return reject(error);
      }
      resolve();
    });
  });
};

// 创建文件夹
export const createMkdir = (url: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    fs.mkdir(url, (error) => {
      if (error) {
        return reject(error);
      }
      resolve();
    });
  });
};

// 读取文件
export const readFile = (url: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    fs.readFile(url, (error, data) => {
      if (error) {
        return reject(error);
      }
      resolve(data.toString());
    });
  });
};
