# Vue3 project code generation tool

## vue3 项目代码生成工具

- 商务合作微信 Shi1426591905
- [gitee](https://gitee.com/ZY_RAN)

## 1. Illustrate（说明）

<p>1. You can generate corresponding. vue. css. API. interface files based on their names for quick development.</p>
<p> 可以根据名称生成对应的.vue .css .api .interface 文件 便于快速开发</p>

<p>2. Can generate corresponding interfaces based on JSON.</p>
<p>  可以根据 JSON 生成对应 Interface</p>

## 2. Preview（预览）

### Create Templates Code（生成模板代码）
![GIF Image](https://gitee.com/ZY_RAN/vue3-create-code/raw/master/src/assets/templates.gif)


### Json Change Interface（Json 转换 Interface）
![GIF Image](https://gitee.com/ZY_RAN/vue3-create-code/raw/master/src/assets/interface.gif)
